using Autofac;

namespace autofac_test
{
    public interface IContext
    {
    }

    public class RootContainer
    {
        private IContext _context;
        private ILifetimeScope _scope;
        public IRoot Root { get; private set; }

        public RootContainer(
            IContext context,
            ILifetimeScope scope
        ) {
            _context = context;
            _scope = scope.BeginLifetimeScope(builder => builder.RegisterInstance(_context).As<IContext>() );
            Root = _scope.Resolve<IRoot>();
        }
    }

    public interface IRoot
    {
        IDepA DepA { get; }
        IDepB DepB { get; }
    }

    public interface IDepA
    {
        IDepAA DepAa { get; }
        IDepAB DepAb { get; }
    }

    public interface IDepB
    {
        IDepBA DepBa { get; }
        IDepBB DepBb { get; }
    }

    public interface IDepAA
    {
    }

    public interface IDepAB
    {
    }

    public interface IDepBA
    {
    }

    public interface IDepBB
    {
    }


    public class Context : IContext
    {
    }

    public class Root : IRoot
    {
        public IContext Context { get; }
        public IDepA DepA { get; }
        public IDepB DepB { get; }

        public Root(
            IContext context,
            IDepA depA,
            IDepB depB)
        {
            this.Context = context;
            this.DepA = depA;
            this.DepB = depB;
        }
    }

    public class DepA : IDepA
    {
        public DepA(
            IContext context,
            IDepAA depAa,
            IDepAB depAb)
        {
            this.Context = context;
            this.DepAa = depAa;
            this.DepAb = depAb;
        }

        public IContext Context { get; }

        public DepA(IContext context)
        {
            this.Context = context;
        }

        public IDepAA DepAa { get; }
        public IDepAB DepAb { get; }
    }

    public class DepB : IDepB
    {
        public DepB(
            IContext context,
            IDepBA depAa,
            IDepBB depAb)
        {
            this.Context = context;
            this.DepBa = depAa;
            this.DepBb = depAb;
        }

        public IContext Context { get; }
        public IDepBA DepBa { get; }
        public IDepBB DepBb { get; }
    }

    public class DepAA : IDepAA
    {
        public DepAA(
            IContext context)
        {
            this.Context = context;
        }

        public IContext Context { get; }
    }

    public class DepAB : IDepAB
    {
        public DepAB(
            IContext context)
        {
            this.Context = context;
        }

        public IContext Context { get; }
    }

    public class DepBA : IDepBA
    {
        public DepBA(
            IContext context)
        {
            this.Context = context;
        }

        public IContext Context { get; }
    }

    public class DepBB : IDepBB
    {
        public DepBB(
            IContext context)
        {
            this.Context = context;
        }

        public IContext Context { get; }
    }
}