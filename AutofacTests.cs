using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Core;
using NUnit.Framework;

namespace autofac_test
{
    public class AutofacTests
    {
        IContainer _container;
        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Root>().As<IRoot>();
            builder.RegisterType<DepA>().As<IDepA>();
            builder.RegisterType<DepB>().As<IDepB>();
            builder.RegisterType<DepAA>().As<IDepAA>();
            builder.RegisterType<DepAB>().As<IDepAB>();
            builder.RegisterType<DepBA>().As<IDepBA>();
            builder.RegisterType<DepBB>().As<IDepBB>();
            builder.RegisterType<RootContainer>().AsSelf();
            _container = builder.Build();
        }

        [TestCase(typeof(IRoot)), Ignore("Упадёт")]
        [TestCase(typeof(IDepAA))]
        public void PassParametersInResolve(Type resolvingType)
        {
            var callContext = new Context();

            IEnumerable<Parameter> parameters = new List<Parameter>
            {
                new TypedParameter(callContext.GetType().GetInterfaces().First(), callContext )
            };

            using var scope = _container.BeginLifetimeScope();
            var root = scope.Resolve(resolvingType, parameters);

            Assert.NotNull(root);
        }
        
        [Test]
        public void PassParametersInResolveInjectLifetimeScope()
        {
            var resolvingType = typeof(RootContainer);

            using var scope = _container.BeginLifetimeScope();

            var callContext1 = new Context();
            IEnumerable<Parameter> parameters1 = new List<Parameter>
            {
                new TypedParameter(callContext1.GetType().GetInterfaces().First(), callContext1 )
            };

            var callContext2 = new Context();
            IEnumerable<Parameter> parameters2 = new List<Parameter>
            {
                new TypedParameter(callContext2.GetType().GetInterfaces().First(), callContext2 )
            };

            var rootContainer1 = (RootContainer) scope.Resolve(resolvingType, parameters1);
            var rootContainer2 = (RootContainer) scope.Resolve(resolvingType, parameters2);
            
            var root1 = (Root)rootContainer1.Root;
            var depA = (DepA)root1.DepA;
            var depAA = (DepAA)root1.DepA.DepAa;
            var depAB = (DepAB)root1.DepA.DepAb;

            var root2 = (Root)rootContainer2.Root;

            Assert.AreNotEqual(root1, root2);
            Assert.AreNotEqual(root1.Context, root2.Context);
            Assert.AreEqual(callContext1, root1.Context);
            Assert.AreEqual(root1.Context, depA.Context);
            Assert.AreEqual(depA.Context, depAA.Context);
            Assert.AreEqual(depA.Context, depAB.Context);
            Assert.AreEqual(depA.Context, depAB.Context);
        }
        
        [TestCase(typeof(IRoot))]
        [TestCase(typeof(IDepAA))]
        public void PassParametersInScope(Type resolvingType)
        {
            var callContext = new Context();
            
            IEnumerable<Parameter> parameters = new List<Parameter>
            {
                new TypedParameter(typeof(IContext), callContext )
            };
            
            using var scope = _container.BeginLifetimeScope(b =>
            {
                foreach (var p in parameters.OfType<TypedParameter>())
                {
                    b.RegisterInstance(p.Value).As(p.Type);
                }
            });
            
            var root = scope.Resolve(resolvingType, parameters);

            Assert.NotNull(root);
        }

        [Test]
        public void RegisterInScope()
        {
            var callContext = new Context();

            using var scope = _container.BeginLifetimeScope(builder => 
                builder.RegisterInstance<IContext>(callContext)
            );

            var root = scope.Resolve<IRoot>() as Root;
            
            Assert.NotNull(root);

            var depA = (root.DepA as DepA);
            var depAa = (depA.DepAa as DepAA);

            Assert.AreSame(root.Context, depA.Context);
            Assert.AreSame(root.Context, depAa.Context);
        }


        [Test]
        public void ResolveFromContainer_ShoudFail()
        {
            using var scope = _container.BeginLifetimeScope(builder => builder.RegisterInstance<IContext>(new Context()));
            Assert.Throws<Autofac.Core.DependencyResolutionException>( () => _container.Resolve<IRoot>() );
        }
    }
}